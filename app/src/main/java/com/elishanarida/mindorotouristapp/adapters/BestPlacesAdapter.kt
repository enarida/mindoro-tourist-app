package com.elishanarida.mindorotouristapp.adapters

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.elishanarida.mindorotouristapp.R
import com.elishanarida.mindorotouristapp.items.BestPlacesItem
import com.facebook.drawee.view.SimpleDraweeView
import com.mikepenz.fastadapter.items.AbstractItem

class BestPlacesAdapter(
        val item: BestPlacesItem
) : AbstractItem<BestPlacesAdapter, BestPlacesAdapter.ViewHolder>() {

    //The unique ID for this type of item
    override fun getType(): Int = R.id.type_bestPlaces

    //The layout to be used for this type of item
    override fun getLayoutRes(): Int = R.layout.template_list

    // displays data on screen
    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>?) {
        super.bindView(holder, payloads)

        holder.title.text = item.title
        holder.description.text = item.description

        val uri = Uri.parse(item.imageUrl)
        val imageView = holder.image
        imageView.setImageURI(uri)

    }

    //Init the viewHolder for this Item
    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var title: TextView = view.findViewById(R.id.title)
        var description: TextView = view.findViewById(R.id.description)
        var image: SimpleDraweeView = view.findViewById(R.id.featured_image)
    }
}
