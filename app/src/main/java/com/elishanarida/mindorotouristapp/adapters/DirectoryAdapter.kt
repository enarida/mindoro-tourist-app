package com.elishanarida.mindorotouristapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.elishanarida.mindorotouristapp.R
import com.elishanarida.mindorotouristapp.items.DirectoryItem
import com.mikepenz.fastadapter.items.AbstractItem

class DirectoryAdapter(
        val item: DirectoryItem
) : AbstractItem<DirectoryAdapter, DirectoryAdapter.ViewHolder>() {

    //The unique ID for this type of item
    override fun getType(): Int = R.id.type_directory

    //The layout to be used for this type of item
    override fun getLayoutRes(): Int = R.layout.template_directory

    // displays data on screen
    override fun bindView(holder: ViewHolder, payloads: MutableList<Any>?) {
        super.bindView(holder, payloads)

        holder.department.text = item.department
        holder.number.text = item.number

    }

    //Init the viewHolder for this Item
    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    //The viewHolder used for this item. This viewHolder is always reused by the RecyclerView so scrolling is blazing fast
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var department: TextView = view.findViewById(R.id.department)
        var number: TextView = view.findViewById(R.id.number)
    }
}
