package com.elishanarida.mindorotouristapp.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.elishanarida.mindorotouristapp.R
import com.elishanarida.mindorotouristapp.adapters.BoatScheduleAdapter
import com.elishanarida.mindorotouristapp.detailed.DetailedBoatSchedule
import com.elishanarida.mindorotouristapp.items.BoatScheduleItem
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.fragment_boat_schedule.*
import org.json.JSONArray
import org.json.JSONObject

class BoatScheduleFragment : Fragment() {

    private val httpClient = AsyncHttpClient()
    private val apiUrl = "https://sheetsu.com/apis/v1.0su/695fbb4eb5b2"
    private val adapter = FastItemAdapter<BoatScheduleAdapter>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_boat_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun setupUI() {

        val recycler = view?.findViewById<RecyclerView>(R.id.boatSchedule_recyclerView)
        recycler?.layoutManager = LinearLayoutManager(context)
        recycler?.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener {
            refreshItems()
        }

        loadItems()

        adapter.withOnClickListener { _, _, item, _ ->
            val intent = Intent(context, DetailedBoatSchedule::class.java)
            intent.putExtra("item", item.item)
            startActivity(intent)
            true
        }
    }

    private fun loadItems() {
        httpClient.get(apiUrl, object : JsonHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>, response: JSONArray) {

                adapter.clear()

                for (i in 0 until response.length()) {
                    val item = BoatScheduleItem.fromJson(response.getJSONObject(i))
                    adapter.add(BoatScheduleAdapter(item))
                }
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                println(throwable?.localizedMessage)
                if (context != null) {
                    Toast.makeText(context, "Please check your internet connection. Swipe down to reload.", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun refreshItems() {
        loadItems()
        swipeRefreshLayout.isRefreshing = false
    }
}
