package com.elishanarida.mindorotouristapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.elishanarida.mindorotouristapp.fragments.*
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val newsFragment = NewsFragment()
    private val festivalEventsFragment = FestivalEventsFragment()
    private val municipalitiesFragment = MunicipalitiesFragment()
    private val pasalubongCenterFragment = PasalubongFragment()
    private val boatSchedulesFragment = BoatScheduleFragment()
    private val directoryFragment = DirectoryFragment()
    private val aboutFragment = AboutFragment()

    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setupDrawer()
        setFragment(newsFragment)
        Fresco.initialize(this)

        auth = FirebaseAuth.getInstance()

        signIn()
    }

    private fun signIn() {
        auth!!.signInAnonymously()
                .addOnCompleteListener(this, { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("Main Activity", "signInAnonymously:success")
                        val user = auth!!.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("Main Activity", "signInAnonymously:failure", task.exception)
                        Toast.makeText(this@MainActivity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                    }
                })
    }

    //   Navigation Drawer
    private fun setupDrawer() {

        val actionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
                val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            }
        }

        drawer_layout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
    }

    //   Setup Fragment
    private fun setFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentPlaceholder, fragment)
                .commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                setFragment(newsFragment)
                title = "Mindoro Tourist App"
            }
            R.id.nav_best_places -> {
                setFragment(municipalitiesFragment)
                title = "Municipalities"
            }
            R.id.nav_festival_and_events -> {
                setFragment(festivalEventsFragment)
                title = "Festival and Events"
            }
            R.id.nav_pasalubong_center -> {
                setFragment(pasalubongCenterFragment)
                title = "Pasalubong Center"
            }
            R.id.nav_boat_schedules -> {
                setFragment(boatSchedulesFragment)
                title = "Boat Schedules"
            }
            R.id.nav_directory -> {
                setFragment(directoryFragment)
                title = "Directory"
            }
            R.id.nav_about -> {
                setFragment(aboutFragment)
                title = "About Us"
            }
            R.id.nav_nearby_hotels -> {
                val intent = Intent(this, NearbyHotelActivity::class.java)
                startActivity(intent)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
