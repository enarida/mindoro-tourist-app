package com.elishanarida.mindorotouristapp

import android.os.AsyncTask
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException

class GetNearbyHotelsData: AsyncTask<Any, String, String>(){

    var googlePlacesData:String? = null
    var mMap: GoogleMap? = null
    var url:String? = null

    override fun doInBackground(vararg objects:Any): String {
        mMap = objects[0] as GoogleMap
        url = objects[1] as String

        val downloadURL = DownloadURL()
        try{
            googlePlacesData = downloadURL.readUrl(url!!)
        }
        catch (e: IOException){
            e.printStackTrace()
        }

        return googlePlacesData!!
    }

    override fun onPostExecute(result: String) {
        val nearbyPlaceList: List<HashMap<String,String>>
        val parser = DataParser()
        nearbyPlaceList = parser.parse(result)
        showNearbyPlaces(nearbyPlaceList)
    }

    private fun showNearbyPlaces(nearbyPlaceList: List<HashMap<String,String>>){

        for (i in 0 until nearbyPlaceList.size){
            val markerOptions = MarkerOptions()
            val googlePlace = nearbyPlaceList[i]

            val placeName = googlePlace["place_name"]
            val vicinity = googlePlace["vicinity"]
            val lat = java.lang.Double.parseDouble(googlePlace["lat"])
            val lng = java.lang.Double.parseDouble(googlePlace["lng"])

            val latLng = LatLng(lat,lng)
            markerOptions.position(latLng)
            markerOptions.title("$placeName : $vicinity")
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))

            mMap!!.addMarker(markerOptions)
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(10f))
        }
    }
}