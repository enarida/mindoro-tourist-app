package com.elishanarida.mindorotouristapp.detailed

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.elishanarida.mindorotouristapp.R
import com.elishanarida.mindorotouristapp.items.PasalubongItem
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.detailed_pasalubong.*
import android.view.MenuInflater



class DetailedPasalubong : AppCompatActivity() {

    private var item: PasalubongItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detailed_pasalubong)
        item = intent.extras.getParcelable("item")
        setupUI()

    }

    private fun setupUI() {

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)

        title = "Pasalubong Center"
        titleName.text = item?.title ?: ""
        description.text = item?.description ?: ""
        location.text = item?.location ?: ""
        contactNumber.text = item?.contact ?: ""

        val image: SimpleDraweeView = this.findViewById(R.id.img)
        val uri = Uri.parse(item?.imageUrl) ?: ""
        image.setImageURI(uri as Uri?)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.pasalubong_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
