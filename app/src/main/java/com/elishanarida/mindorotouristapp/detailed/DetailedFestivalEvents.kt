package com.elishanarida.mindorotouristapp.detailed

import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.elishanarida.mindorotouristapp.R
import com.elishanarida.mindorotouristapp.items.FestivalEventsItem
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.detailed_news.*

class DetailedFestivalEvents : AppCompatActivity() {

    private var item: FestivalEventsItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detailed_festivalevents)

        item = intent.extras.getParcelable("item")
        setupUI()

    }

    private fun setupUI() {

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)

        title = item?.title ?: ""
        titleName.text = item?.title ?: ""
        description.text = item?.description ?: ""

        val image: SimpleDraweeView = this.findViewById(R.id.img)
        val uri = Uri.parse(item?.imageUrl) ?: ""
        image.setImageURI(uri as Uri?)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}