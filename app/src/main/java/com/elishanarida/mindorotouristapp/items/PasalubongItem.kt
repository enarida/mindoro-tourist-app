package com.elishanarida.mindorotouristapp.items

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject

@SuppressLint("ParcelCreator")
@Parcelize
data class PasalubongItem(
        val title: String,
        val description: String,
        val imageUrl: String,
        val location: String,
        val mapUrl: String,
        val contact: String
) : Parcelable {

    companion object {
        fun fromJson(json: JSONObject) = PasalubongItem(
                json.getString("title"),
                json.getString("description"),
                json.getString("photoUrl"),
                json.getString("location"),
                json.getString("mapUrl"),
                json.getString("contact")
        )
    }
}