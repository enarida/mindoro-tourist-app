package com.elishanarida.mindorotouristapp.items

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject

@SuppressLint("ParcelCreator")
@Parcelize
data class FestivalEventsItem(
        val title: String,
        val description: String,
        val imageUrl: String
) : Parcelable {

    companion object {
        fun fromJson(json: JSONObject) = FestivalEventsItem(
                json.getString("title"),
                json.getString("description"),
                json.getString("photoUrl")
        )
    }
}