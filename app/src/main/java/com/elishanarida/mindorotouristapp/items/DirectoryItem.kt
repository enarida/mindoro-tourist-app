package com.elishanarida.mindorotouristapp.items

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject

@SuppressLint("ParcelCreator")
@Parcelize
data class DirectoryItem(
        val department: String,
        val number: String
) : Parcelable {

    companion object {
        fun fromJson(json: JSONObject) = DirectoryItem(
                json.getString("department"),
                json.getString("phoneNumber")
        )
    }
}