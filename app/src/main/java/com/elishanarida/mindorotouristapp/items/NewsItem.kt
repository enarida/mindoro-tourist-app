package com.elishanarida.mindorotouristapp.items

import android.annotation.SuppressLint
import android.os.Parcelable
import org.json.JSONObject
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class NewsItem(
        val title: String,
        val description: String,
        val imageUrl: String
) : Parcelable {

    companion object {
        fun fromJson(json: JSONObject) = NewsItem(
                json.getString("title"),
                json.getString("description"),
                json.getString("photoUrl")
        )
    }
}